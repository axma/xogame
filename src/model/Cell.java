package model;

public class Cell {
    private State currentState = State.EMPTY;

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }
}
