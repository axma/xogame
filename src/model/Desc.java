package model;

public class Desc {
    public static int SIZE = 3;
    private Cell[][] cells = new Cell[SIZE][SIZE];

    public Desc() {
        for (int i = 0; i<SIZE ; i++ ) {
            for (int j = 0; j<SIZE ; j++ ) {
                cells[i][j] = new Cell();
            }
        }
    }

    public void show() {
        for (int i = 0; i<SIZE ; i++ ) {
            for (int j = 0; j<SIZE ; j++ ) {
                System.out.println(cells[i][j].getCurrentState());
            }
        }
    }
}
